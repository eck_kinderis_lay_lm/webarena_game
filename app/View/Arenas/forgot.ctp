<!DOCTYPE html>
<html>

<head>
	<?php $this->assign ('title','Forgot Password'); ?>
</head>

<body>
 <img id='titre' src='../img/Thrones4.png'>
 
 <div class="divofforgt">
  <h3>Set New Password</h3>
  <?php
  echo $this->Form->create('NewPdw', array(
      'class' => 'form-horizontal', 
      'role' => 'form',
      'inputDefaults' => array(
          'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
          'div' => array('class' => 'form-group'),
          'class' => array('form-control'),
          'label' => array('class' => 'col-sm-1 control-label'),
          'between' => '<div class="col-sm-3">',
          'after' => '</div>',
          'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
          )));
  echo $this->Form->input('email');
  echo $this->Form->input('New password', array(
      'type'=>'password',
      'required'));
  echo $this->Form->end(array(
      'class'=>'col-sm-offset-1 btn btn-warning',
      'label'=>'Submit'
      ));
      ?>
  </div>
  <img id='footersight' src='../img/Thrones3.png'>
  
</body>

</html>