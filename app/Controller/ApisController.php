<?php 

App::uses('AppController', 'Controller');

/**
 * Main controller of our small application
 *
 * @author ...
 */
class ApisController extends AppController{
    
	public $uses = array('Player', 'Fighter', 'Surroundings', 'Event');

	public function fighterview($id){

		$this->layout = 'ajax';
		//find the fighter
		$f = $this->Fighter->findByid($id);
		$fightersight = $f['Fighter']['skill_sight'];
		$x = $f['Fighter']['coordinate_x'];
		$y = $f['Fighter']['coordinate_y'];

		//get the surroundings the current fighter can see (manhattan distance)
		$surroundingsInSight = $this->Surroundings->query(
			"SELECT * from Surroundings where  
			(SELECT (ABS('$x'-Surroundings.coordinate_x)
				+ ABS('$y'-Surroundings.coordinate_y))) <='$fightersight'");


		$this->set('datassurroundings', $surroundingsInSight);


	//get the fighters the current fighter can see (manhattan distance)

		$fightersInSight = $this->Fighter->query(
			"SELECT * from Fighters where  
			(SELECT (ABS('$x'-fighters.coordinate_x)
				+ ABS('$y'-Fighters.coordinate_y))) <='$fightersight'");


		$this->set('datasfighters', $fightersInSight);	

	}

	function checkFighterHere($x, $y)
            {
                $fighterhere = 
                $this->Fighter->find('first', 
                  array( 'conditions' => 
                    array('Fighter.coordinate_x' => $x , 'Fighter.coordinate_y' => $y )));
                if(empty($fighterhere)) return false;
                else return true;
            }


	public function fighterdomove($fighterId,$direction)
	{

		$width_arena = 15;
		$height_arena = 10;

                //Collect position and fix work ID
                //$datas = $this->read(null, $fighterId);
		$datas = $this->Fighter->findByid($fighterId);


               //Do direction modification
                //if north
		if ($direction == 'north')	{
                    //if trying to go out of the arena
			if ($datas['Fighter']['coordinate_y']+1 > $height_arena) {
				$message = array('live','You just hit a wall');

			}else
			{
				$newcoord = array(
					'x' => $datas['Fighter']['coordinate_x'],
					'y' => $datas['Fighter']['coordinate_y'] + 1);
				$message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);
			}
		}


		elseif($direction == 'south') {
                    //if trying to go out of the arena

			if ($datas['Fighter']['coordinate_y']-1 < 1) {
				$message = array('live','You just hit a wall');

			}else
			{
				$newcoord = array(
					'x' => ($datas['Fighter']['coordinate_x']),
					'y' => ($datas['Fighter']['coordinate_y'] - 1));

				$message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);

			}
		}
		elseif($direction == 'east') {
                    //if trying to go out of the arena
			if ($datas['Fighter']['coordinate_x']+1 > $width_arena) {

				$message = array('live','You just hit a wall');

			}else
			{
				$newcoord = array(
					'x' => ($datas['Fighter']['coordinate_x'] + 1),
					'y' => $datas['Fighter']['coordinate_y']);

				$message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);


			}
		}     elseif($direction == 'west') {
                    //if trying to go out of the arena
			if (($datas['Fighter']['coordinate_x']-1) < 1) {
				$message = array('live','You just hit a wall');

			}else
			{
				$newcoord = array(
					'x' => ($datas['Fighter']['coordinate_x'] - 1),
					'y' => $datas['Fighter']['coordinate_y']);

				$message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);


			}
		} else {
                    //return array('live','Please, choose a well direction');
			$this->set('evenement',$message);
		}
		$this->set('evenement',$message);
		$datas = $this->Fighter->findByid($fighterId);
		$this->set('datasfighters',$datas);
	}





	function eventNewPosition($x,$y,$fighterId)
	{




		$fightermoving = $this->Fighter->read(null, $fighterId);
		$fighterdatas = $this->Fighter->findByid($fighterId);


		$fightername = $fighterdatas['Fighter']['name'];
                //$this->set('coordinate_y', $newcoord['y']);
                            // $this->save();

            //check if there is a Fighter on the square
		if($this->checkFighterHere($x, $y))
		{
                    //$this->Session->setFlash('You just hit an other fighter');
			return array('live','You just hit an other fighter, you can not move here');
                }//check if there is a model in the square
                elseif($this->Surroundings->checkSurroundingsHere($x,$y))
                {
                    //found wichsurround is in the square and return the actions corresponding 
                	$wichsurround = $this->Surroundings->checkWhichSurroundingshere($x,$y);
                        //if it s a pillar
                	if($wichsurround == 'pillar')
                	{
                           // $this->Session->setFlash('You can not walk on a pillar');
                		return array('live','You can not walk on a pillar');
                	}
                        //if it s invisible trap
                	elseif($wichsurround == 'invisibletrap')
                	{
                		$this->Fighter->set('coordinate_x', $x);
                		$this->Fighter->set('coordinate_y', $y);
                		$this->Fighter->save();
                          // $this->Session->setFlash('You have been killed by an invisible trap');
                		$evenement = "The fighter $fightername have been killed 
                		because of an invisible trap";
                		$this->Event->createEvent($evenement,$fightermoving['Fighter']['coordinate_x'],
                			$fightermoving['Fighter']['coordinate_y']);
                		$this->Fighter->delete($fighterId);
                		return array('killed','You have been killed by an invisible trap, please create an other fighter');
                	}
                	elseif($wichsurround == 'invisiblemonster')
                	{
                		$this->Fighter->set('coordinate_x', $x);
                		$this->Fighter->set('coordinate_y', $y);
                		$this->Fighter->save();
                		$this->Fighter->delete($fighterId);
                		$evenement = "The fighter $fightername have been killed 
                		because of an invisible monster";
                		$this->Event->createEvent($evenement,$fightermoving['Fighter']['coordinate_x'],
                			$fightermoving['Fighter']['coordinate_y']);
                		$this->Fighter->delete($fighterId);

                           // $this->Session->setFlash('You have been killed by an invisible monster');
                		return array('killed','You have been killed by an invisible monster, please create an other fighter');

                	}
                }
                else{
                	$this->Fighter->set('coordinate_x', $x);
                	$this->Fighter->set('coordinate_y', $y);
                	$this->Fighter->save();
                	return array('live','move');
                }

            }
        
        
        public function fighterdoattack($fighterId, $direction){
    
    
        //Collect position and fix work ID
                $datas = $this->Fighter->read(null, $fighterId);
                $hitpoints = $datas['Fighter']['skill_strength'];
                $toDelete = 0;
        //Attack and Do direction modification
                if ($direction == 'north') {

                    $datas2 = $this->Fighter->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y'] + 1),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x']))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($this->Fighter->attackWorks($hitpoints, $datas2['Fighter']['skill_strength'])) {
                            $this->Fighter->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->Fighter->save();
                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->Fighter->save();
                        }

                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->Fighter->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->Fighter->delete($toDelete);
                    }

                    $this->Fighter->save();
                } elseif ($direction == 'south') {

                    $datas2 = $this->Fighter->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y'] - 1),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x']))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($this->Fighter->attackWorks($hitpoints, $datas2['Fighter']['skill_strength'])) {
                            $this->Fighter->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->Fighter->save();
                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->Fighter->save();
                        }

                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->Fighter->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->Fighter->delete($toDelete);
                    }

                    $this->Fighter->save();
                } elseif ($direction == 'east') {

                    $datas2 = $this->Fighter->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y']),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x'] + 1))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($this->Fighter->attackWorks($hitpoints, $datas2['Fighter']['skill_strength'])) {
                            $this->Fighter->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->Fighter->save();
                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->Fighter->save();
                        }

                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->Fighter->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->Fighter->delete($toDelete);
                    }

                    $this->Fighter->save();
                } elseif ($direction == 'west') {

                    $datas2 = $this->Fighter->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y']),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x'] - 1))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($this->Fighter->attackWorks($hitpoints, $datas2['Fighter']['skill_strength'])) {
                            $this->Fighter->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->Fighter->save();
                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->Fighter->save();
                        }

                        $datas2 = $this->Fighter->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->Fighter->read(null, $fighterId);
                            $this->Fighter->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->Fighter->save();

                            $toDelete = $defendersId;
                        }
                    } 
                    if ($toDelete != 0) {
                        $this->Fighter->delete($toDelete);
                    }

                    $this->Fighter->save();
                } else {
                    return false;
                }

                if ($toDelete != 0) {
                    $fighterName = $datas['Fighter']['name'];
                    $opponentName = $datas2['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];


                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' killed ' . $opponentName, $x, $y);

                    $this->Fighter->delete($toDelete); 
                }elseif (!empty($datas2)) {
                    $fighterName = $datas['Fighter']['name'];
                    $opponentName = $datas2['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];
                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' attacked ' . $opponentName, $x, $y);
                }  else {
                    $fighterName = $datas['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];
                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' missed the mark', $x, $y);
                }


                while ($this->Fighter->FighterNeedsLevelup($fighterId)) {
                    $this->Fighter->LevelUpFighter($fighterId);
                }

        //Save modification
                $this->Fighter->save();
                return true;
            }

        }

    



?>
