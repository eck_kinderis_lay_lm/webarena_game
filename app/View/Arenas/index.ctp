<!DOCTYPE html>
<html>
<head>
	<?php $this->assign ('title','Rules'); ?>
</head>
<body>

	<img id='titre' src='img/Thrones4.png'>

	<div id='rules'>
		<h2>Rules</h2>

		<ol>
			<li>A fighter is in a board arena to a position X, Y. This position can not be out of the dimension
			of the arena. One fighter per square. One arena per site. </li>
			<li>A fighter begins with the following skills: view = 0, strength = 1, lifepoint = 3. It appears in a free random position. </li>
			<li>Parameterized constants should be : width (x) of the arena, length (y) of the arena. Delivery values will be: x=15, y=10.</li>
			<li>View skill determines how far a fighter can see, in Manhattan Distance (= x + y) . Thus, only fighters and surroundings are displayed on the relevant page. 0 corresponds to the current square. </li>
			<li>Strength skill determines how many life points its opponent loses when a fighter succeed a n attack. </li>
			<li>When a fighter life points reach 0, he is removed from play. A player whose fighter was removed from the game is invited to create a new one. </li>
			<li>An attack action is successful if a random value between 1 and 20 (20 sided dice) is greater than a threshold calculated as follows: 10 + level of attack - level of the attacker. </li>
			<li>Improvement: For each successful attack, a fighter gains 1 experience point. If the attack kills the opponent, the fighter wins as many experience point as the level of the defeated opponent. For four experience points, the fighter changes level and may choose to increase one of its skills: view (+1), strength(+1) or lifepoints(+3). </li>
			<li>Every action causes the creation of an event with a clear description. For example: "Jonh attacks Bill and hits." </li>
		</ol>
	</div>

	<img id='footerindex' src='img/Thrones3.png'>
</body>

</html>

