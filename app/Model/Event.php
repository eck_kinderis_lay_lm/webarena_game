<?php

App::uses('AppModel', 'Model');

class Event extends AppModel {

    public $uses = array('Player', 'Fighter', 'Event');

    public function createEntry($newFighter) {


        $date = date('Y-m-d H:i:s');
        $datas = array(
            'name' => 'Entry of ' . $newFighter['Fighter']['name'],
            'coordinate_x' => $newFighter['Fighter']['coordinate_x'],
            'coordinate_y' => $newFighter['Fighter']['coordinate_y'],
            'date' => $date
        );
        $this->create('Event');
        $this->save($datas);
    }

    function checkEvent($eventId) {
        $datas = $this->read(null, $eventId);
        return $datas['Event']['name'];
    }

    function addEvent($name, $x, $y) {
        $today = new DateTime(null, new DateTimeZone('America/New_York'));
        $today->setTimezone(new DateTimeZone('Europe/Paris'));    // Another way
        $today = $today->format('Y-m-d H:i:s');
        ;
        $datas = array('Event' => array('name' => $name, 'date' => $today, 'coordinate_x' => $x, 'coordinate_y' => $y));
        $this->create('Event');
        $this->save($datas);
    }

    function eventNotNull($eventId) {
        $datas = $this->read(null, $eventId);
        if (!empty($datas)) {
            return true;
        } else {
            return false;
        }
    }

    function getEvent($eventId) {
        $datas = $this->read(null, $eventId);
        return $datas;
    }

  public function createEvent($quote,$x,$y)
  {
    $date = date('Y-m-d H:i:s');
    $datas = array(
          'name'=> $quote,
          'coordinate_x'=>$x,
          'coordinate_y'=>$y,
          'date'=> $date
            );
        $this->create('Event');
        $this->save($datas);
  }

}

?>
