<!DOCTYPE html>
<html>
<head>
  <?php $this->assign ('title','Vision'); ?>
</head>

<body>

    

    <?php

    if(!empty($eventHappened)){
        echo '<div class="alert alert-info">';
        echo '<div class="container-fluid">';
        echo '<p class="text-center">' . "<strong>" .  "Event: " . "</strong>"  .  $eventHappened['Event']['name'] . "</p>";
        $eventHappened = null;
        echo '</div>';
        echo '</div>';
    }


    if($viewBreeze == true){
        echo '<div class="alert alert-danger">';
        echo '<div class="container-fluid">';
        echo '<p class="text-center">' . "<strong>" .  "Breeze" . "</strong></p>";
        echo '</div>';
        echo '</div>';
    }

    if($viewSmell == true){
        echo '<div class="alert alert-success">';
        echo '<div class="container-fluid">';
        echo '<p class="text-center">' . "<strong>" .  "Smell" . "</strong></p>";
        echo '</div>';
        echo '</div>';
    }


    if($monsterkilled== true){
        echo '<div class="alert alert-success">';
        echo '<div class="container-fluid">';
        echo '<p class="text-center">' . "<strong>" .  "Nicely done: " . "</strong>You killed the invisible monster</p>";
        echo '</div>';
        echo '</div>';
    }


    ?>
    <img id='titre' src='../img/Thrones4.png'>

    <?php


    if ($playerkilled == 'false')
    {


        if($playerhavefighter){

         
       //Table with fighters in present fighters sight
            $nbViewedFighters = count($viewFighter);
            ?>
            
            
            <div id='sighttables'>
                <table id="tablefighter" class="table table-striped table-bordered">
                    <?php

                    echo "<h4>".'Fighters Seen'."</h4>";
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>".'Guild'."</th>";
                    echo "<th>".'Name'."</th>";
                    echo "<th>".'Coordinate x'."</th>";
                    echo "<th>".'Coordinate y'."</th>";
                    echo "</tr>";
                    echo "</thead>";
                    
                    echo "<tbody>";

                    for($i=0; $i<$nbViewedFighters;$i++){
                        echo "<tr>";
                        echo "<td>";
                        if($allguilds[$i]!=NULL){
                         switch($allguilds[$i]['Guild']['name']){
                            case 'playmobil':?>
                            <img class='imgsight' src='../img/playmobil.png'>
                            House Playmobil
                            <?php
                            break;
                            case 'barbie':?>
                            <img class='imgsight' src='../img/barbie.png'>
                            House Barbie
                            <?php
                            break;
                            case 'teddy':?>
                            <img class='imgsight' src='../img/teddy.png'>
                            House Teddy
                            <?php
                            break;
                            case 'lego':?>
                            <img class='imgsight' src='../img/lego.png'>
                            House Lego
                            <?php
                            break;
                            case 'puzzle':?>
                            <img class='imgsight' src='../img/puzzle.png'>
                            House Puzzle
                            <?php
                            break;
                        } 
                    }
                    else{
                        echo "Didn't pledge allegiance to any houses";
                    }
                    
                    echo "</td>";
                    echo "<td>".$viewFighter[$i]['Fighters']['name']."</td>";
                    echo "<td>".$viewFighter[$i]['Fighters']['coordinate_x']."</td>";
                    echo "<td>".$viewFighter[$i]['Fighters']['coordinate_y']."</td>";
                    
                    echo "</tr>"; 
                }
                echo "</tdody>";
                echo "</table>";

                
        //Table with surroundings in present fighters sight
                $nbViewedSurroundings = count($viewSurround);
                ?>
                <table id="tablesurr" class="table table-striped table-bordered">
                    <?php
                    echo "<h4>".'Surroundings Seen'."</h4>";
                    echo "<tr>";
                    echo "<th>".'Type'."</th>";
                    echo "<th>".'Coordinate x'."</th>";
                    echo "<th>".'Coordinate y'."</th>";
                    echo "</tr>";
                    
                    for($j=0; $j<$nbViewedSurroundings;$j++){
                        if($viewSurround[$j]['Surroundings']['type']=='pillar'){
                         echo "<tr>";

                         echo "<td>".$viewSurround[$j]['Surroundings']['type']."</td>";
                         echo "<td>".$viewSurround[$j]['Surroundings']['coordinate_x']."</td>";
                         echo "<td>".$viewSurround[$j]['Surroundings']['coordinate_y']."</td>";
                         
                         echo "</tr>";  
                     }
                     
                 }

                 echo "</table>";?>
             </div>
             <div id='sight'>
                <div id='sightform'> 
                    <?php     
        //Form to move fighter
                    echo $this->Form->create('Fightermove', array(
                        'class' => 'form-horizontal', 
                        'role' => 'form',
                        'inputDefaults' => array(
                            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                            'div' => array('class' => 'form-group'),
                            'class' => array('form-control'),
                            'label' => array('class' => 'col-sm-1 control-label'),
                            'between' => '<div class="col-lg-3">',
                            'after' => '</div>',
                            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                            )));
                    echo $this->Form->input('direction', array ('options' => array ('north' => 'north', 'east' => 'east', 'south' => 'south', 'west' => 'west'), 'default' => 'east'));
                    echo $this->Form->end(array(
                        'class'=>'col-sm-offset-1 btn btn-warning',
                        'label'=>'Move'
                        ));
                        ?>
                    </div> 
                    <div id='sightform'>
                        <?php
    //Form to attack in a chosen direction
                        echo $this->Form->create('Fighterattack', array(
                            'class' => 'form-horizontal', 
                            'role' => 'form',
                            'inputDefaults' => array(
                                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                'div' => array('class' => 'form-group'),
                                'class' => array('form-control'),
                                'label' => array('class' => 'col-sm-1 control-label'),
                                'between' => '<div class="col-lg-3">',
                                'after' => '</div>',
                                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                )));
                        echo $this->Form->input('direction', array ('options' => array ('north' => 'north', 'east' => 'east', 'south' => 'south', 'west' => 'west'), 'default' => 'east'));
                        echo $this->Form->end(array(
                            'class'=>'col-sm-offset-1 btn btn-warning',
                            'label'=>'Attack'
                            ));
                            ?> 
                        </div> 
                        <div id='sightform'>
                           <?php 
    // form to level up
                           echo $this->Form->create('SkillLevelup', array(
                            'class' => 'form-horizontal', 
                            'role' => 'form',
                            'inputDefaults' => array(
                                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                'div' => array('class' => 'form-group'),
                                'class' => array('form-control'),
                                'label' => array('class' => 'col-sm-1 control-label'),
                                'between' => '<div class="col-lg-3">',
                                'after' => '</div>',
                                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                )));
                           echo $this->Form->input ('skill', array ('options' => array ('strength' => 'strength', 'sight' => 'sight', 'health' => 'health'), 'default' => 'strength'));
                           echo $this->Form->end(array(
                            'class'=>'col-sm-offset-1 btn btn-warning',
                            'label'=>'Up'
                            ));
                           
                            ?>
                        </div>   
                    </div> 
                    <?php 
                }
                else{
                 pr('Please create a fighter. You need one to begin the conquest of the throne of games!');
             }
         }
         ?>
         <img id='footersight' src='../img/Thrones3.png'>
     </body>
     </html>
