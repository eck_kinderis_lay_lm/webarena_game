<?php

App::uses ('AppModel', 'Model');
App::uses('Event','Model');



class Fighter extends AppModel {

 public $displayField = 'name';

 public $belongsTo = array (

     'Player' => array (

         'className' => 'Player',

         'foreignKey' => 'player_id'
         ),
     'Guild' => array( 
        'className' => 'Guild',

        'foreignKey' => 'guild_id')

     );

             /**
              *
              * @todo prevent moving out of bound of the arena
              * @todo prevent moving to an occupied space
              *
              */
             function assignGuild($fighterId, $GuildName){

       /* $datas = $this->read(null, $fighterId);
        app::uses('Guild', 'Model');
        $this->set('guild_id', $this->Guild->GuildNameToId($GuildName) );
       */ 
        $datas = $this->read(null, $fighterId);
        app::uses('Guild', 'Model');
        $guilOb = new Guild();
        
        $this->set('guild_id', $guilOb->GuildNameToId($GuildName));
        $this->save();
    }

    function removeFromPlay($fighterId) {
        $data = $this->read(NULL, $fighterId);

        if ($data['Fighter']['current_health'] == 0) {
            unset($this);
        }
    }

    function addSkill($fighterId, $SkillName) {

        if ($this->canAddSkill($fighterId)) {


            if ($SkillName == 'strength') {
                $this->increaseStrength($fighterId);
            } elseif ($SkillName == 'sight') {
                $this->increaseSight($fighterId);
            } elseif ($SkillName == 'health') {
                $this->increaseHealth($fighterId);
            }
        } else {
            pr("Impossible to add a skill, you need to level up");
        }
    }

    function increaseStrength($fighterId) {
        $datas = $this->read(null, $fighterId);
        $this->set('skill_strength', $datas['Fighter']['skill_strength'] + 1);
        $this->save();
    }

    function increaseSight($fighterId) {
        $datas = $this->read(null, $fighterId);
        $this->set('skill_sight', $datas['Fighter']['skill_sight'] + 1);
        $this->save();
    }

    function increaseHealth($fighterId) {
        $datas = $this->read(null, $fighterId);
        $this->set('skill_health', $datas['Fighter']['skill_health'] + 3);
        $this->save();
    }

    function canAddSkill($fighterId) {
        $datas = $this->read(null, $fighterId);
        $level = $datas['Fighter']['level'];
        $health = $datas['Fighter']['skill_health'];
        $sight = $datas['Fighter']['skill_sight'];
        $strength = $datas['Fighter']['skill_strength'];
        $skillLevel = ($health / 3 ) + ($sight - 2) + ($strength - 1) + 1;

        if ($level > $skillLevel) {
            return true;
        } else {
            return false;
        }
    }

    function LevelUpFighter($fighterId) {

        $datas = $this->read(null, $fighterId);
        $this->set('level', $datas['Fighter']['level'] + 1);
        $this->save();
    }

    function FighterNeedsLevelup($fighterId) {
        $datas = $this->read(null, $fighterId);
        $level = $datas['Fighter']['level'];
        $xp = $datas['Fighter']['xp'];

        if (($level + 1) * 4 <= ( $xp )) {
            return true;
        } else {
            return false;
        }
    }



    function attackWorks($lvattacker, $lvattacked) {
        $z = rand(1, 20);
        if ($z >= ( 10 + $lvattacked - $lvattacker ))
            return true;
        else {
            pr("Your attack missed");
            return false;
        }
    }
    
    
    function eventNewPosition($x,$y,$fighterId)
    {
        App::uses('Surroundings','Model');

        $surroundModel=new Surroundings();

        App::uses('Event','Model');

        $diaryModel=new Event();

        $fightermoving = $this->read(null, $fighterId);
        $fighterdatas = $this->findByid($fighterId);

        
        $fightername = $fighterdatas['Fighter']['name'];
                //$this->set('coordinate_y', $newcoord['y']);
                            // $this->save();

            //check if there is a Fighter on the square
        if($this->checkFighterHere($x, $y))
        {
                    //$this->Session->setFlash('You just hit an other fighter');
            return array('live','You just hit an other fighter, you can not move here');
                }//check if there is a model in the square
                elseif($surroundModel->checkSurroundingsHere($x,$y))
                {
                    //found wichsurround is in the square and return the actions corresponding 
                    $wichsurround = $surroundModel->checkWhichSurroundingshere($x,$y);
                        //if it s a pillar
                    if($wichsurround == 'pillar')
                    {
                           // $this->Session->setFlash('You can not walk on a pillar');
                        return array('live','You can not walk on a pillar');
                    }
                        //if it s invisible trap
                    elseif($wichsurround == 'invisibletrap')
                    {
                        $this->set('coordinate_x', $x);
                        $this->set('coordinate_y', $y);
                        $this->save();
                          // $this->Session->setFlash('You have been killed by an invisible trap');
                        $evenement = "The fighter $fightername have been killed 
                        because of an invisible trap";
                        $diaryModel->createEvent($evenement,$fightermoving['Fighter']['coordinate_x'],
                            $fightermoving['Fighter']['coordinate_y']);
                        $this->delete($fighterId);
                        return array('killed','You have been killed by an invisible trap, please create an other fighter');
                    }
                    elseif($wichsurround == 'invisiblemonster')
                    {
                        $this->set('coordinate_x', $x);
                        $this->set('coordinate_y', $y);
                        $this->save();
                        $this->delete($fighterId);
                        $evenement = "The fighter $fightername have been killed 
                        because of an invisible monster";
                        $diaryModel->createEvent($evenement,$fightermoving['Fighter']['coordinate_x'],
                            $fightermoving['Fighter']['coordinate_y']);
                        $this->delete($fighterId);

                           // $this->Session->setFlash('You have been killed by an invisible monster');
                        return array('killed','You have been killed by an invisible monster, please create an other fighter');

                    }
                }
                else{
                    $this->set('coordinate_x', $x);
                    $this->set('coordinate_y', $y);
                    $this->save();
                    return array('live','move');
                }

            }

            function doMove($fighterId, $direction){
                $width_arena = 15;
                $height_arena = 10;

                //Collect position and fix work ID
                //$datas = $this->read(null, $fighterId);
                $datas = $this->findByid($fighterId);


               //Do direction modification
                //if north
                if ($direction == 'north')	{
                    //if trying to go out of the arena
                    if ($datas['Fighter']['coordinate_y']+1 > $height_arena) {
                        $message = array('live','You just hit a wall');

                    }else
                    {
                        $newcoord = array(
                            'x' => $datas['Fighter']['coordinate_x'],
                            'y' => $datas['Fighter']['coordinate_y'] + 1);
                        $message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);
                    }
                }


                elseif($direction == 'south') {
                    //if trying to go out of the arena

                    if ($datas['Fighter']['coordinate_y']-1 < 1) {
                        $message = array('live','You just hit a wall');

                    }else
                    {
                        $newcoord = array(
                            'x' => ($datas['Fighter']['coordinate_x']),
                            'y' => ($datas['Fighter']['coordinate_y'] - 1));

                        $message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);

                    }
                }
                elseif($direction == 'east') {
                    //if trying to go out of the arena
                    if ($datas['Fighter']['coordinate_x']+1 > $width_arena) {

                        $message = array('live','You just hit a wall');

                    }else
                    {
                        $newcoord = array(
                            'x' => ($datas['Fighter']['coordinate_x'] + 1),
                            'y' => $datas['Fighter']['coordinate_y']);

                        $message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);


                    }
                }     elseif($direction == 'west') {
                    //if trying to go out of the arena
                    if (($datas['Fighter']['coordinate_x']-1) < 1) {
                        $message = array('live','You just hit a wall');

                    }else
                    {
                        $newcoord = array(
                            'x' => ($datas['Fighter']['coordinate_x'] - 1),
                            'y' => $datas['Fighter']['coordinate_y']);

                        $message = $this->eventNewPosition($newcoord['x'],$newcoord['y'],$fighterId);


                    }
                } else {
                    return array('live','Please, choose a well direction');
                }
                return $message;
            }





            function addFighter($player_id, $name) {
                $x;
                $y;
                App::uses('Surroundings', 'Model');


                $surroundModel = new Surroundings();
                $somethinghere = true;
                while ($somethinghere) {
                    $x = rand(1, 15);
                    $y = rand(1, 10);
                    $somethinghere = $this->checkFighterHere($x, $y);
                    if (!$somethinghere) {
                        $somethinghere = $surroundModel->checkSurroundingsHere($x, $y);
                    }

                }

                $data = array(
                    'name' => $name,
                    'player_id' => $player_id,
                    'coordinate_x' => $x,
                    'coordinate_y' => $y,
                    'level' => 1,
                    'xp' => 0,
                    'skill_sight' => 2,
                    'skill_strength' => 1,
                    'current_health' => 3
                    );

        // prepare the model for adding a new entry
                $this->create('Fighter');

        // save the data

                $this->save($data);

                return $this->id;
            }



            public function fighterviewFighters($fighterid) {
                app::uses('Guild', 'Model');
                    $newGuild = new Guild();
        //find the fighter

                $f = $this->read(null, $fighterid);
                $f = $this->findByid($fighterid);
                $fightersight = $f['Fighter']['skill_sight'];
                $x = $f['Fighter']['coordinate_x'];
                $y = $f['Fighter']['coordinate_y'];

        //get the fighters the current fighter can see (manhattan distance)
                $fightersInSight = $this->query(
                    "SELECT * from Fighters where  
                    (SELECT (ABS('$x'-fighters.coordinate_x)
                        + ABS('$y'-Fighters.coordinate_y))) <='$fightersight'");
                        
                return $fightersInSight;
            }

            public function fighterviewSurround($fighterid) {
        //find the fighter

                $f = $this->read(null, $fighterid);
                $f = $this->findByid($fighterid);
                $fightersight = $f['Fighter']['skill_sight'];
                $x = $f['Fighter']['coordinate_x'];
                $y = $f['Fighter']['coordinate_y'];

        //get the fighters the current fighter can see (manhattan distance)
                $surroundsInSight = $this->query(
                    "SELECT * from Surroundings where  
                    (SELECT (ABS('$x'-surroundings.coordinate_x)
                        + ABS('$y'-surroundings.coordinate_y))) <='$fightersight'");

                return $surroundsInSight;
            }

        

            public function fighterviewBreeze($fighterid) {
        //find the fighter

                $f = $this->read(null, $fighterid);
                $f = $this->findByid($fighterid);
                $fightersight = $f['Fighter']['skill_sight'];
                $x = $f['Fighter']['coordinate_x'];
                $y = $f['Fighter']['coordinate_y'];


        //get the fighters the current fighter can see (manhattan distance)
               $surroundsInSight = $this->query(
                    "SELECT * from Surroundings where  
                    ((SELECT (ABS('$x'-surroundings.coordinate_x)
                        + ABS('$y'-surroundings.coordinate_y))) <= 1)
                AND surroundings.type LIKE '%invisibletrap%'");
                if(!empty($surroundsInSight)){
                    return true;
                }
                else return false;
            }


     public function fighterviewSmell($fighterid) {
        //find the fighter

                $f = $this->read(null, $fighterid);
                $f = $this->findByid($fighterid);
                $fightersight = $f['Fighter']['skill_sight'];
                $x = $f['Fighter']['coordinate_x'];
                $y = $f['Fighter']['coordinate_y'];


        //get the fighters the current fighter can see (manhattan distance)
               $surroundsInSight = $this->query(
                    "SELECT * from Surroundings where  
                    ((SELECT (ABS('$x'-surroundings.coordinate_x)
                        + ABS('$y'-surroundings.coordinate_y))) <= 1)
                AND surroundings.type LIKE '%invisiblemonster%'");
                if(!empty($surroundsInSight)){
                    return true;
                }
                else return false;
            }

            


            function checkFighterHere($x, $y)
            {
                $fighterhere = 
                $this->find('first', 
                  array( 'conditions' => 
                    array('Fighter.coordinate_x' => $x , 'Fighter.coordinate_y' => $y )));
                if(empty($fighterhere)) return false;
                else return true;
            }





            

            function doAttack($fighterId, $direction) {

        //Collect position and fix work ID
                $datas = $this->read(null, $fighterId);
                $hitpoints = $datas['Fighter']['skill_strength'];
                $level = $datas['Fighter']['level'];
                $toDelete = 0;
        //Attack and Do direction modification
                if ($direction == 'north') {

                    $datas2 = $this->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y'] + 1),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x']))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->read(null, $defendersId);


                        if ($this->attackWorks($level, $datas2['Fighter']['level'])) {
                            $this->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->save();
                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->save();
                        }

                        $datas2 = $this->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->delete($toDelete);
                    }

                    $this->save();
                } elseif ($direction == 'south') {

                    $datas2 = $this->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y'] - 1),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x']))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->read(null, $defendersId);


                        if ($this->attackWorks($level, $datas2['Fighter']['level'])) {
                            $this->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->save();
                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->save();
                        }

                        $datas2 = $this->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->delete($toDelete);
                    }

                    $this->save();
                } elseif ($direction == 'east') {

                    $datas2 = $this->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y']),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x'] + 1))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->read(null, $defendersId);


                        if ($this->attackWorks($level, $datas2['Fighter']['level'])) {
                            $this->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->save();
                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->save();
                        }

                        $datas2 = $this->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->save();

                            $toDelete = $defendersId;
                        }
                    } 

                    if ($toDelete != 0) {
                        $this->delete($toDelete);
                    }

                    $this->save();
                } elseif ($direction == 'west') {

                    $datas2 = $this->find('first', array('conditions' =>
                        array('Fighter.coordinate_y' => ($datas['Fighter']['coordinate_y']),
                            'Fighter.coordinate_x' => ($datas['Fighter']['coordinate_x'] - 1))));

                    if (count($datas2) != 0) {

                        $defendersId = $datas2['Fighter']['id'];
                        $datas2 = $this->read(null, $defendersId);


                        if ($this->attackWorks($level, $datas2['Fighter']['level'])) {
                            $this->set('current_health', $datas2['Fighter']['current_health'] - $hitpoints);
                            $this->save();
                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + 1);
                            $this->save();
                        }

                        $datas2 = $this->read(null, $defendersId);


                        if ($datas2['Fighter']['current_health'] < 1) {

                            $defendersLevel = $datas['Fighter']['level'];

                            $datas = $this->read(null, $fighterId);
                            $this->set('xp', $datas['Fighter']['xp'] + $defendersLevel);
                            $this->save();

                            $toDelete = $defendersId;
                        }
                    } 
                    if ($toDelete != 0) {
                        $this->delete($toDelete);
                    }

                    $this->save();
                } else {
                    return false;
                }

                if ($toDelete != 0) {
                    $fighterName = $datas['Fighter']['name'];
                    $opponentName = $datas2['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];


                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' killed ' . $opponentName, $x, $y);

                    $this->delete($toDelete); 
                }elseif (!empty($datas2)) {
                    $fighterName = $datas['Fighter']['name'];
                    $opponentName = $datas2['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];
                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' attacked ' . $opponentName, $x, $y);
                }  else {
                    $fighterName = $datas['Fighter']['name'];
                    $x = $datas['Fighter']['coordinate_x'];
                    $y = $datas['Fighter']['coordinate_y'];
                    app::uses('Event', 'Model');
                    $newEvent = new Event();
                    $newEvent->addEvent($fighterName . ' missed the mark', $x, $y);
                }


                while ($this->FighterNeedsLevelup($fighterId)) {
                    $this->LevelUpFighter($fighterId);
                }

        //Save modification
                $this->save();
                return true;
            }

        }

        ?>
