<?php

App::uses ('AppModel', 'Model');

class Surroundings extends AppModel {

	public $uses = array('Player', 'Fighter', 'Event', 'Surroundings');

	function checkSurroundingsHere($x,$y)
	{
	$surroundhere = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
    if(empty($surroundhere)) return false;
    else return true;
	}

	function checkWhichSurroundingshere($x,$y)
	{
		$surroundhere = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
		if(!(empty($surroundhere)))
		{
			return $surroundhere['Surroundings']['type'];
		}else {return false;}
	}

	function findCoordSurroundings($x_current,$y_current,$step)
	{
		App::uses('Fighter','Model');

		$fighterModel=new Fighter();
		$coord = array(
			'x',
			'y');
		$fighterhere = true;
		//check if there is no fighter in the random case
		while($fighterhere==true)
		{
		$coord['x']=rand(1,$step)+$x_current;
       	$coord['y']=rand(1,$step)+$y_current;
       	// return true if there is a fighter in the case (x,y)
       	//return false if there is no fighter in the case (x,y)
		$fighterhere = $fighterModel->checkFighterHere($coord['x'], $coord['y']);
		if(!$fighterhere)
		{
			$fighterhere = $this->checkSurroundingsHere($coord['x'], $coord['y']);
		}
		}
		return $coord;
	}



	function createsurroundings()
	{
		//$this->loadModel('Fighter');
		$data = $this->find('all');

		//y
      	$height = 10;
      	//x
      	$width = 15;
     
      if(empty($data))
      {
      	$step = 5;
      	

      	for($x_current = 0; $x_current < $width; $x_current = $x_current + $step)
      	{
      		for($y_current = 0; $y_current < $height; $y_current = $y_current + $step)
      		{
      			$coord = $this->findCoordSurroundings($x_current, $y_current, $step);
      			$this->createpillar($coord['x'],$coord['y']);

      			$coord = $this->findCoordSurroundings($x_current, $y_current, $step);
      			$this->createinvisibletrap($coord['x'],$coord['y']);

      		}
      	}

      	$coord = $this->findCoordRand($width,$height);
      			$this->createinvisiblemonster($coord['x'],$coord['y']);
      }
      elseif(empty($this->find('all',array('conditions'=> array('type' => 'invisiblemonster')))))
      {
      	$coord = $this->findCoordRand($width,$height);
      			$this->createinvisiblemonster($coord['x'],$coord['y']);
      }
	}

	function findCoordRand($width,$height)
	{
		App::uses('Fighter','Model');

		$fighterModel=new Fighter();
		$coord = array(
			'x',
			'y');
		$fighterhere = true;
		//check if there is no fighter in the random case
		while($fighterhere==true)
		{
		$coord['x']=rand(1,$width);
       	$coord['y']=rand(1,$height);
       	// return true if there is a fighter in the case (x,y)
       	//return false if there is no fighter in the case (x,y)
		$fighterhere = $fighterModel->checkFighterHere($coord['x'], $coord['y']);
		if(!$fighterhere)
		{
			$fighterhere = $this->checkSurroundingsHere($coord['x'], $coord['y']);
		}
		}
		return $coord;
	}

	function createpillar($x,$y)
	{
		$data = array(
        'type' => 'pillar',
        'coordinate_x' => $x,
        'coordinate_y' => $y
        );

        $this->create('Surroundings');

// save the data
$this->save($data);
      	
 	 }
      
      function createinvisibletrap($x,$y)
	{
      
      	
		$data = array(
        'type' => 'invisibletrap',
        'coordinate_x' => $x,
        'coordinate_y' => $y
        );

        $this->create('Surroundings');

		// save the data
		$this->save($data);
      
	
	}

	      function createinvisiblemonster($x,$y)
	{
		
      
		$data = array(
        'type' => 'invisiblemonster',
        'coordinate_x' => $x,
        'coordinate_y' => $y
        );

        $this->create('Surroundings');

		// save the data
		$this->save($data);
      
	
	}

	function attackmonster($x,$y,$direction)
	{
	      	if($direction=='south')
	      	{
	      		$y = $y -1;
	      		if($this->checkWhichSurroundingshere($x,$y) == 'invisiblemonster'){
	      			$monster = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
	      			$this->delete($monster['Surroundings']['id']);
	      			return true;
	      		}else return false;
	      		
	      	}elseif($direction=='north')
	      	{
	      		$y = $y +1;
	      		if($this->checkWhichSurroundingshere($x,$y) == 'invisiblemonster'){
	      			$monster = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
	      			$this->delete($monster['Surroundings']['id']);
	      			return true;
	      		}else return false;
	      	}elseif($direction=='east')
	      	{
	      		$x = $x +1;
if($this->checkWhichSurroundingshere($x,$y) == 'invisiblemonster'){
	      			$monster = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
	      			$this->delete($monster['Surroundings']['id']);
	      			return true;
	      		}else return false;	      	}
	      		elseif($direction=='west')
	      	{
	      		$x = $x -1;
if($this->checkWhichSurroundingshere($x,$y) == 'invisiblemonster'){

	      			$monster = $this->find('first', 
    	array('conditions' => 
    		array('Surroundings.coordinate_x' => $x, 
    			'Surroundings.coordinate_y' => $y )));
	      			$this->delete($monster['Surroundings']['id']);
	      			return true;
	      		}else {	
return false;}	      	}
	

}
}

?>