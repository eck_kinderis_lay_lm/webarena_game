<?php

App::uses ('AppModel', 'Model');


class Player extends AppModel {

	public $validate = array(
		'email'=> 'email',
		'password'=>array(
			)
	);
	
	function createNew($email, $password) {

		$data = array(
			'email' => $email,
			'password' => $password
		);


		$this->create('Player');
		$this->save($data);
	}

	function checkLogin($email, $password){
		
		$candidate=$this->findByEmail($email);

		if($candidate['Player']['password']==Security::hash($password, NULL, true))
			return $candidate['Player']['id'];

		else 
			return false;
	}

	function setNewPassword($email,$password){
		$password = Security::hash($password, NULL, true);
		
		if($this->findByEmail($email)!=NULL){
			$candidate=$this->findByEmail($email);
			$candidate['Player']['password']=$password;

			$this->save($candidate);
			pr('Password set');	
		}
		else{
			pr('This email is not assigned to any existing account.');
		}
		

	}

	function checkIfPlayerHaveFighter($id)
	{
		App::uses('Fighter','Model');
		$fighterModel=new Fighter();
		$f = $fighterModel->find('first', 
			array('conditions' => array('Fighter.player_id' => $id)));
		if(empty($f)){return false;}
		else return true;
	}

	function fighterofPlayer($id)
	{
		App::uses('Fighter','Model');
		$fighterModel=new Fighter();
		$thefighter = $fighterModel->find('first', 
		array('conditions' => array('Fighter.player_id' => $id)));
		return $thefighter;
	}
}

?>