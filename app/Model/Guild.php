<?php

App::uses('AppModel', 'Model');

class Guild extends AppModel {

    public $uses = array('Fighter', 'Event', 'Guild');

    function checkGuildsIfEmpty() {
        $datas = $this->find('all');

        if (empty($datas)) {
            return true;
        } else {
            return false;
        }
    }
    
    function createAllGuildsAtOnce(){
                
        $datas1 = array( 'name' => 'playmobil'); 
        $datas2 = array( 'name' => 'barbie'); 
        $datas3 = array( 'name' => 'lego'); 
        $datas4 = array( 'name' => 'teddy'); 
        $datas5 = array( 'name' => 'puzzle'); 

        
        $this->create('Guild');
        $this->save($datas1);
        $this->create('Guild');
        $this->save($datas2);
        $this->create('Guild');
        $this->save($datas3);
        $this->create('Guild');
        $this->save($datas4);
        $this->create('Guild');
        $this->save($datas5);
        
    }

    function GuildNameToId($GuildName){
        $datas = $this->find('first', array( 'conditions' => array('Guild.name' => $GuildName)));
        return $datas['Guild']['id'];
    }

    
}


?>