<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?//php echo $cakeDescription ?>
		<?php echo $this->fetch('title'); ?>
	</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('webarena');
		echo $this->Html->css('bootstrap.min.css');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>

<body>

	<div id="container-fluid">

		<div id="header">
			<!-- Fixed navbar -->
  			<nav class="navbar navbar-inverse navbar-fixed-top">
  				<div class="container">
    				<div class="navbar-header">
      					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        					<span class="icon-bar"></span>
        					<span class="icon-bar"></span>
        					<span class="icon-bar"></span>
      					</button>
      				<a class="navbar-brand navbar-left" href="#">Toy Arena</a>
    				</div>
    				<div class="collapse navbar-collapse">
      					<ul class="nav navbar-nav">
        					<li><?php echo $this->html-> link ('Home', array ('controller' => '/', 'action' => '/')) ; ?></li>
        					<li><?php echo $this->html-> link ('Rules', array ('controller' => 'Arenas', 'action' => 'index')) ; ?></li>
        					<li><?php echo $this->html-> link ('Vision', array ('controller' => 'Arenas', 'action' => 'sight')) ; ?></li>
        					<li><?php echo $this->html-> link ('Fighter', array ('controller' => 'Arenas', 'action' => 'fighter')) ; ?></li>
							<li><?php echo $this->html-> link ('Diary', array ('controller' => 'Arenas', 'action' => 'diary')) ; ?></li>
							<li><?php echo $this->html-> link ('Connection', array ('controller' => 'Arenas', 'action' => 'login')) ; ?></li>
      					</ul>
      					<ul class="nav navbar-nav navbar-right">
					        <li><?php echo $this->html-> link('Sign Out', array ('controller'=>'Arenas', 'action'=>'logout')); ?></li>
      					</ul>
    				</div>
  				</div>
			</nav>
		</div>

		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		
	</div>
	
	<!--<?php echo $this->element('sql_dump'); ?>-->

	<div class="navbar navbar-inverse navbar-fixed-bottom">
		<div class="container-fluid">
			<p class="navbar-text pull-right">Christopher Eck | Marius Kinderis | Ophélie Le Mentec | Céline Lay SI1-12-DG</p>
			<p class="navbar-text pull-left">http://www.throneofgamesece.com</p>

		</div>
	</div>


</body>
</html>
  


