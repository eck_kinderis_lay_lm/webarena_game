<!DOCTYPE html>
<html>
<head>
	<?php $this->assign ('title','Connection'); ?>
</head>
<body>
  <img id='titre' src='../img/Thrones4.png'>

  <div id='login'>
    <?php

    if($PlayerConnected == true){
      //echo $this->Session->flash();
    }

    else{
      echo "<h3>".'Create New Account'."</h3>";
      echo $this->Form->create('Subscribe', array(
        'class' => 'form-horizontal', 
        'role' => 'form',
        'inputDefaults' => array(
          'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
          'div' => array('class' => 'form-group'),
          'class' => array('form-control'),
          'label' => array('class' => 'col-sm-1 control-label'),
          'between' => '<div class="col-lg-3">',
          'after' => '</div>',
          'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
          )));
      echo $this->Form->input('email');
      echo $this->Form->input('password', array(
        'required'));
      echo $this->Form->end(array(
        'class'=>'col-sm-offset-1 btn btn-warning',
        'label'=>'Subscribe'
        ));

      echo "<h3>".'Sign In'."</h3>" ;
      echo $this->Form->create('Login', array(
        'class' => 'form-horizontal', 
        'role' => 'form',
        'inputDefaults' => array(
          'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
          'div' => array('class' => 'form-group'),
          'class' => array('form-control'),
          'label' => array('class' => 'col-sm-1 control-label'),
          'between' => '<div class="col-sm-3">',
          'after' => '</div>',
          'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
          )));
      echo $this->Form->input('email');
      echo $this->Form->input('password', array(
      'required'));?>
      <div class="col-sm-offset-1">
        <?php
        echo $this->html->link('Forgot password?', array(
          'controller'=>'Arenas',
          'action'=>'forgot'));      
          ?>
        </div>

        <?php
        echo $this->Form->end(array(
          'class'=>'col-sm-offset-1 btn btn-warning',
          'label'=>'Sign In'
          ));

        echo $this->Form->create('LoginFb', array(
          'class' => 'form-horizontal', 
          'role' => 'form',
          'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-1 control-label'),
            'between' => '<div class="col-sm-3">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
            )));
        echo $this->Form->hidden(null);
        echo $this->Form->end(array(
          'class'=>'col-sm-offset-1 btn btn-warning',
          'label'=>'Sign In with Facebook'
          ));
        echo $this->Form->create('LoginGoogle', array(
          'class' => 'form-horizontal', 
          'role' => 'form',
          'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-1 control-label'),
            'between' => '<div class="col-sm-3">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
            )));
        echo $this->Form->hidden(null);
        echo $this->Form->end(array(
          'class'=>'col-sm-offset-1 btn btn-warning',
          'label'=>'Sign In with Google'
          ));
      }
      ?>
    </div>
    <img id='footerlogin' src='../img/Thrones3.png'>
  </body>
  </html>

