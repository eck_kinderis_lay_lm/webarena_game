<?php 

//  $components = array( 'Session' );


/**
 * Main controller of our small application
 *
 * @author ...
 */
App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
App::uses ('Security', 'Utility');

// Inclusion of library files for Facebook and Google
// Replaced directory of file with ".."
  /*require_once('../app/fblogin/lib/Facebook/FacebookSession.php');
  require_once('../app/fblogin/lib/Facebook/FacebookRequest.php');
  require_once('../app/fblogin/lib/Facebook/FacebookResponse.php');
  require_once('../app/fblogin/lib/Facebook/FacebookSDKException.php');
  require_once('../app/fblogin/lib/Facebook/FacebookRequestException.php');
  require_once('../app/fblogin/lib/Facebook/FacebookRedirectLoginHelper.php');
  require_once('../app/fblogin/lib/Facebook/FacebookAuthorizationException.php');
  require_once('../app/fblogin/lib/Facebook/GraphObject.php');
  require_once('../app/fblogin/lib/Facebook/GraphUser.php');
  require_once('../app/fblogin/lib/Facebook/GraphSessionInfo.php');
  require_once('../app/fblogin/lib/Facebook/Entities/AccessToken.php');
  require_once('../app/fblogin/lib/Facebook/HttpClients/FacebookCurl.php');
  require_once('../app/fblogin/lib/Facebook/HttpClients/FacebookHttpable.php');
  require_once('../app/fblogin/lib/Facebook/HttpClients/FacebookCurlHttpClient.php');

  require ( '../gplus-lib/vendor/autoload.php');
  */
// Use Namespaces

  /*use Facebook\FacebookSession;
  use Facebook\FacebookRedirectLoginHelper;
  use Facebook\FacebookRequest;
  use Facebook\FacebookResponse;
  use Facebook\FacebookSDKException;
  use Facebook\FacebookRequestException;
  use Facebook\FacebookAuthorizationException;
  use Facebook\GraphObject;
  use Facebook\GraphUser;
  use Facebook\GraphSessionInfo;
  use Facebook\FacebookHttpable;
  use Facebook\FacebookCurlHttpClient;
  use Facebook\FacebookCurl;
  */

  class ArenasController extends AppController
  {

    //Google IDs
    /*const CLIENT_ID = '176826513977-b3rjrhejitq4ipj5eb2sn0nl09dc0kq4.apps.googleusercontent.com';
    const CLIENT_SECRET = 'DGuCDgW8Frmeo6UdpE6Gf_PY';
    const REDIRECT_URI = 'http://www.throneofgamesece.com';*/
    /**
     * index method : first page
     *
     * @return void
     */
    public $uses = array('Player', 'Fighter', 'Event', 'Surroundings', 'Guild');


    public $components = array('Session');
    

    
    public function beforeFilter(){
      if($this->Session->read('Connected') == NULL && $this->action!='login' && $this->action!='index' && $this->action!='forgot'){
        $this->redirect(array(
          'controller' => 'Arenas', 
          'action' => 'login'));
      }      
    }

    public function index(){

    }

    public function login()
    {
        //Session Initialization
      $this->Session->write('Toto1',false);
      $this->set('PlayerConnected',false);

      if(!empty($this->Session->read('Connected'))){
        $this->set('PlayerConnected',true);

        $this->Session->setFlash('You are already connected.');
      }

        //Subscribe
      if(isset($this->request->data['Subscribe']))
      {

        $email=$this->request->data['Subscribe']['email'];
        $password=$this->request->data['Subscribe']['password'];

            //Check if the user entered email and password
        if($email != NULL && $password != NULL){
          $password = Security::hash($password, NULL, true);
          $this->Player->createNew($email, $password);
          $this->request->data('Subscribe.password',$password);
          $player = $this->Player->findByEmail($email);

              //Login Automated
          if(!empty($player)){
            $this->Session->write('Connected',$player['Player']['id']);
            $this->set('PlayerConnected',true);
            $this->Session->setFlash('Welcome to Toy Arena! Get ready to fight for the throne of games!');
          }
        }
        else{
          $this->Session->setFlash('Please enter valide email and password');
        }  


      }

        //Login
      if(isset($this->request->data['Login']))
      {  
        $email=$this->request->data['Login']['email'];
        $password=$this->request->data['Login']['password'];

          //Check that user entered email and password
        if($email != NULL && $password != NULL){      
          $ok=$this->Player->checkLogin($email, $password);

          if($ok!=NULL){
            $this->Session->write('Connected',$ok);
            $this->set('PlayerConnected',true);
            $this->Session->setFlash('You have succesfully been connected');
          }
        }
        else{
          $this->Session->setFlash('Please enter your email and password');
        }  

      }

        //Login with Facebook
        //if(isset($this->request->data['LoginFb'])){



        // Define useful variables
            /*$app_id = '852149384901851';
            $app_secret = '0de1aa07972fc12130a5a628f70c8d82Reset';
            $redirect_url = 'http://throneofgamesece.com';

        // Initialization
            FacebookSession::setDefaultApplication($app_id, $app_secret);
            $helper = new FacebookRedirectLoginHelper($redirect_url);
            $sess = $helper->getSessionFromRedirect();

            // Check if Facebook session exists
            if(isset($_SESSION['fb_token'])){
              $sess = new FacebookSession($_SESSION['fb_token']);
            }

        // Facebook session exists
            if(isset($sess)){
            // Store the token in the php session
            $_SESSION['fb_token'] = $sess->getToken();    
            // Create request object, execute and capture response
            $request = new FacebookRequest($sess,'GET','/me');
            // From response get graph object
            $response = $request->execute();
            $graph = $response->getGraphObject(GraphUser::classname());
            // Use graph object methods to get user details 
            $email = $graph->getProperty('email');
            
            $password = 'facebook';
            $password = Security::hash($password, NULL, true);
            $this->Player->createNew($email, $password);
            $ok=$this->Player->checkLogin($email, $password);
                
            if($ok!=NULL){
              $this->Session->write('Connected',$ok);
            } 
            }

          // Else echo login
          else{
            $this->redirect($helper->getLoginUrl(array('email'))); 
          //}     
        //}

        //Login with google 
        //if(isset($this->request->data['LoginGoogle'])){
          //Initialization

          $client = new Google_Client();
          $client->setClientId(CLIENT_ID);
          $client->setClientSecret(CLIENT_SECRET);  
          $client->setRedirectUri(REDIRECT_URI);
          $client->setScopes('email');

          $plus = new Google_Service_Plus($client);



          // Get token after authentification
          /*if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
          }

          // Check if we have access to the token
          /*if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
            $me = $plus->people->get('me');
            $email =  $me['emails'][0]['value'];
            
            //Connection to our website
            $password = 'google';
            $password = Security::hash($password, NULL, true);
            $this->Player->createNew($email, $password);
            $ok=$this->Player->checkLogin($email, $password);
                
            if($ok!=NULL){
            $this->Session->write('Connected',$ok);
          } 

        } else {
          // get the login url   
          $authUrl = $client->createAuthUrl();
        }
      }*/


    }

    public function forgot(){

      if(isset($this->request->data['NewPdw'])){
        if($this->Player->validates()){
          $password=$this->request->data['NewPdw']['New password'];
          $email=$this->request->data['NewPdw']['email'];
          $this->Player->setNewPassword($email,$password);
        }
      }
    }

    public function logout(){

      $this->Session->destroy();
    }

    public function fighter()
    {

      $fighterP = $this->Player->fighterofPlayer($this->Session->read('Connected'));

      if($this->Guild->checkGuildsIfEmpty()){
        $this->Guild->createAllGuildsAtOnce();
      }


      if(!empty($fighterP)){
        $this->set('playerhavefighter',true);
        $this->set('fighterOfPlayer', $fighterP);
        $this->set('viewFighter',$this->Fighter->fighterviewFighters($fighterP['Fighter']['id']));
        $this->set('viewBreeze',$this->Fighter->fighterviewBreeze($fighterP['Fighter']['id']));
        $this->set('viewSmell',$this->Fighter->fighterviewSmell($fighterP['Fighter']['id']));

      }else {
        $this->set('playerhavefighter',false);
      }

      if(isset($this->request->data['Fighteradd']))
      {
        $theid=$this->Fighter->addFighter($this->Session->read('Connected'), $this->request->data['Fighteradd']['name']);
        $fight = $this->Fighter->findById($theid);
        $this->Event->createEntry($fight);
        $file = $this->request->data['Fighteradd']['Upload file'];
        $this->Session->setFlash('Your fighter has successfully been created');
        move_uploaded_file($this->data['Fighteradd']['Upload file']['tmp_name'],
            IMAGES . 'avatars' . DS . 'avatar_' . $theid . '.png');
        $this->Fighter->assignGuild($fighterP, $this->request->data['Fighteradd']['Guild']);
        $this->redirect(array(
          'controller' => 'Arenas', 
          'action' => 'fighter'));

      } elseif(isset($this->request->data['FighterLevelUp']))
        {
          $this->Fighter->levelUpFighter($this->request->data['FighterLevelUp']['Fighter ID']);  
        } 
    }


    public function sight()
    {              
      $listGuilds;
      $message[0] = 'something';
      $this->set('monsterkilled',false);

      //Fill the database with surroundings if there is no surroundings in the database
      $this->Surroundings->createsurroundings();

      $fighterP = $this->Player->fighterofPlayer($this->Session->read('Connected'));


      if(!empty($fighterP)){
        $this->set('playerhavefighter',true);
        $this->set('fighterOfPlayer', $fighterP);
        //display the enemies that the fighter can see
        $this->set('viewFighter',$this->Fighter->fighterviewFighters($fighterP['Fighter']['id']));
        $fightersseen = $this->Fighter->fighterviewFighters($fighterP['Fighter']['id']);
            
        $this->set('viewSurround',$this->Fighter->fighterviewSurround($fighterP['Fighter']['id']));

        $nbViewedFighters = count($fightersseen);
        $listGuilds;
        for($i=0;$i<$nbViewedFighters;$i++)
        {
          $listGuilds[$i] = $this->Guild->findById($fightersseen[$i]['Fighters']['guild_id']);
        }
        $this->set('allguilds', $listGuilds);

      }else {
        $this->set('playerhavefighter',false);
      }


      if(isset($this->request->data['Fightermove']))
      {
        $message = $this->Fighter->doMove ($fighterP['Fighter']['id'], $this->request->data['Fightermove']['direction']);
        if($message[1] != 'move') $this->Session->setFlash($message[1]);
  
        elseif($message[0]=='killed') 
        {
          $this->set('playerkilled','true');
        }
      }

	     //Attack in a direction (move is working, attack not implemented yet)
      if(isset($this->request->data['Fighterattack']))
      {
        $monsterhere = $this->Surroundings->attackmonster($fighterP['Fighter']['coordinate_x'],
        $fighterP['Fighter']['coordinate_y'],
        $this->request->data['Fighterattack']['direction']);
        if($monsterhere) {$this->set('monsterkilled',true);}
        else {
          $this->Fighter->doAttack($fighterP['Fighter']['id'], $this->request->data['Fighterattack']['direction']);
          $this->set('eventHappened',$this->Event->find('first', array('order' => array('Event.date' => 'desc'))));
        }
      }

      $fighterP = $this->Player->fighterofPlayer($this->Session->read('Connected'));
      if(!empty($fighterP))
      {
        //display the enemies that the fighter can see
        $this->set('viewFighter',$this->Fighter->fighterviewFighters($fighterP['Fighter']['id']));
        $this->set('viewSurround',$this->Fighter->fighterviewSurround($fighterP['Fighter']['id']));
        $this->set('viewBreeze',$this->Fighter->fighterviewBreeze($fighterP['Fighter']['id']));
        $this->set('viewSmell',$this->Fighter->fighterviewSmell($fighterP['Fighter']['id']));

        $fightersseen = $this->Fighter->fighterviewFighters($fighterP['Fighter']['id']);

        $nbViewedFighters = count($fightersseen);
        $listGuilds;
        for($i=0;$i<$nbViewedFighters;$i++)
        {
          $listGuilds[$i] = $this->Guild->findById($fightersseen[$i]['Fighters']['guild_id']);    
        }
        $this->set('allguilds', $listGuilds);

      } else {
        $this->redirect(array(
          'controller' => 'Arenas', 
          'action' => 'fighter'));
      }


      // Level up fighter

      if(isset($this->request->data['SkillLevelup']))
      {
        $this->Fighter->addSkill($fighterP['Fighter']['id'], $this->request->data['SkillLevelup']['skill'] );
      }
      $this->set ('raw', $this->Fighter->find ('all'));

      if($message[0]!='killed')
      {
        $this->set('playerkilled','false');
      }  
    }

    public function diary()
    {
      $this->set('raw',$this->Event->find('all'));
      $this->set('length',$this->Event->find('count'));
      
    }
  }
?>
